FROM node:alpine

# install dependencies
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm ci
COPY . .
RUN npm run-script build

EXPOSE 5000
CMD ["npm", "start"]