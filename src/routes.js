import PageLayout from './PageLayout.svelte';
import Index from './pages/Index.svelte';
import Trips from './pages/Trips.svelte';
import Trip from './pages/Trip.svelte';
import Vehicles from './pages/Vehicles.svelte';
import Operator from './pages/Operator.svelte';
import Vehicle from './pages/Vehicle.svelte';
import Lines from './pages/Lines.svelte';
import Line from './pages/Line.svelte';

const routes = [
  {
    name: '/',
    component: Index,
    layout: PageLayout,
  },
  {
    name: 'trips',
    component: Trips,
    layout: PageLayout,
    nestedRoutes: [
      { name: "id/:id", component: Trip },
    ]
  },
  {
    name: 'vehicles',
    component: Vehicles,
    layout: PageLayout,
    nestedRoutes: [
      { name: "operator/:operatorid", component: Operator },
      { name: "id/:vehicleref", component: Vehicle },
    ]
  },
  {
    name: 'routes',
    component: Lines,
    layout: PageLayout,
    nestedRoutes: [
      { name: "ref/:ref", component: Line, }
    ]
  },
]

export { routes }