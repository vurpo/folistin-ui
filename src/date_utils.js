import { DateTime, Interval } from "luxon";

export function getCurrentTimetableDayRange() {
    let now = DateTime.local();
    // This nonsense is because the timetable day always goes from 03:00 to 02:59
    let start = now.minus({hours:3}).startOf('day').plus({hours:3});
    let end = now.minus({hours:3}).endOf('day').plus({hours:3});
    return Interval.fromDateTimes(start, end);
}