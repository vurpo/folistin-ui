export const liikennoitsijat = {
    "2": "TLO",
    "6": "Jalo Bus",
    "8": "TuKL",
    "11": "Savonlinja",
    "16": "Turkubus",
    "18": "Muurinen",
    "20": "Nyholm",
    "27": "Citybus",
    "28": "Vesma",
    "41": "Länsilinjat",
    "55": "V-S Bussipalvelut",
    "67": "Vainio",
    "68": "Päivölä",
    "69": "Airisto Line",
    "70": "Explore Saimaa",
    "80": "Lauran Bussi",
    "81": "Sa Tilausajot"
};

export function getOperator(busID) {
    if (busID.slice(0,-4) in liikennoitsijat) {
        return (busID.slice(0,-4), liikennoitsijat(busID.slice(0,-4)));
    } else {
        return null;
    }
}

export function getBusNo(busID) {
    return busID.slice(0,-4);
}

export function formatBusID(busID) {
    if (busID.slice(0,-4) in liikennoitsijat) {
        return liikennoitsijat[busID.slice(0,-4)] + ' ' + parseInt(busID.slice(-4));
    } else {
        return busID;
    }
}